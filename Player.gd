extends KinematicBody2D

var MOVE_SPEED = 500

#warning-ignore:unused_argument
func _physics_process(delta):
	var move_dir_x = 0.0
	var move_dir_y = 0.0
	if (Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_up") && !Input.is_action_pressed("move_down")):
		move_dir_x = 1.0
	if (Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_up") && !Input.is_action_pressed("move_down")):
		move_dir_x = -1.0
	if (Input.is_action_pressed("move_up") && !Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_down")):
		move_dir_y = -1.0
	if (Input.is_action_pressed("move_down") && !Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_up") && !Input.is_action_pressed("move_right")):
		move_dir_y = 1.0

	if (Input.is_action_pressed("move_down") && Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_up")):
		move_dir_x = -0.5
		move_dir_y = 0.5
	if (Input.is_action_pressed("move_down") && !Input.is_action_pressed("move_left") && Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_up")):
		move_dir_x = 0.5
		move_dir_y = 0.5
	if (!Input.is_action_pressed("move_down") && Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_right") && Input.is_action_pressed("move_up")):
		move_dir_x = -0.5
		move_dir_y = -0.5
	if (!Input.is_action_pressed("move_down") && !Input.is_action_pressed("move_left") && Input.is_action_pressed("move_right") && Input.is_action_pressed("move_up")):
		move_dir_x = 0.5
		move_dir_y = -0.5
	
#warning-ignore:return_value_discarded
	move_and_slide(Vector2(move_dir_x * MOVE_SPEED, move_dir_y * MOVE_SPEED), Vector2(0, 0))
